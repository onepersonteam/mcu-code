//
// Created by honza on 31.7.16.
//
#include <ESP8266WiFi.h>

#define WLAN_SSID        "ssid"
#define WLAN_PASS        "pass"

int ledPin = D4;
WiFiServer server(80);

void checkWiFiConnection(wl_status_t status);

void setup() {
    Serial.begin(9600);
    delay(10);

    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin, LOW);

    // Connect to WiFi network
    Serial.print("Connecting to ");
    Serial.println(WLAN_SSID);

    WiFi.mode(WIFI_STA);
    wl_status_t status = (wl_status_t) WiFi.begin(WLAN_SSID, WLAN_PASS);
    if (status == WL_NO_SHIELD) {
        Serial.println(F("radio_dead;"));
        return;
    }

    {
        uint8_t dhcpWait = 250; // 25 seconds

        while (dhcpWait > 0) {
            status = (wl_status_t) WiFi.status();
            if (status == WL_CONNECTED) {
                IPAddress address = WiFi.localIP();

                if (INADDR_NONE != address) {
                    Serial.println("ip_received;");

                    dhcpWait = 1;
                } else {
                    status = WL_DISCONNECTED;
                }
            }
            delay(100);
            dhcpWait--;
        }
    }

    checkWiFiConnection(status);

    Serial.println("");
    Serial.println("WiFi connected");

    // Start the server
    server.begin();

    // Print the IP address
    Serial.print("Use this URL to connect: ");
    Serial.print("http://");
    Serial.print(WiFi.localIP());
    Serial.println("/");

}

void loop() {
    // Check if a client has connected
    WiFiClient client = server.available();
    if (!client) {
        checkWiFiConnection(WiFi.status());
        return;
    }

    // Read the first line of the request
    String req = client.readStringUntil('\r');
    Serial.println(req);
    client.flush();

    String response = "OK";

    // Match the request
    if (req.indexOf("/led/change") != -1) {
        digitalWrite(ledPin, !digitalRead(ledPin));
    } else if (req.indexOf("/led/0") != -1) {
        digitalWrite(ledPin, LOW);
    } else if ((req.indexOf("/led/1") != -1)) {
        digitalWrite(ledPin, HIGH);
    } else {
        response = "Error 404: Requested error was not found.";
    }
    // Set GPIO5 according to the request
    client.flush();

    // Send the response to the client
    client.print(response);
    delay(1);

    // The client will actually be disconnected
    // when the function returns and 'client' object is detroyed
}

void checkWiFiConnection(wl_status_t status) {
    if (status != WL_CONNECTED) {
        Serial.println("ESP restart");
        ESP.restart();
    }
}